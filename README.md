# dropbox

A free service that lets you bring your photos, docs, and videos anywhere and share them easily

https://www.dropbox.com

Latest versions discussions:

https://www.dropboxforum.com/t5/Dropbox-desktop-client-builds/bd-p/101003016

Run the following command in case you got errors during "Verifying source file signatures with gpg..."

```
gpg --recv-keys 1C61A2656FB57B7E4DE0F4C1FC918B335044912E
```

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/cloud/dropbox.git
```
